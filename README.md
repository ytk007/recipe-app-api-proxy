# Recipe App API Proxy

NGINX proxy app for our recip app API

## Usage

### Environmental Variables

* `LISTEN_PORT` - Port to listen on (default: `8080`)
* `APP_HOST` - Hostname of the app to forward request to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`)
